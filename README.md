# build-a-bot
```
Build a bot is a little front-end proyect to practice and learn VueJs Fundamentals.
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
